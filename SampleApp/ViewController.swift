//
//  ViewController.swift
//  SampleApp
//
//  Created by James Gartland on 24/03/2017.
//  Copyright © 2017 James Gartland. All rights reserved.
//

import UIKit

class ViewController: CAPViewController {

    @IBOutlet weak var nuanceLabel: UILabel!
    @IBOutlet weak var capitoLabel: UILabel!
    @IBAction func microphoneButton(_ sender: UIButton) {
        onMicrophoneClick();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func speechControllerProcessing(_ transcription: CapitoTranscription!) {
        nuanceLabel.numberOfLines = 0;
        nuanceLabel.text = transcription.firstResult();
    }
    
    override func handleResponse(response: CapitoResponse) {
        capitoLabel.numberOfLines = 0;
        capitoLabel.text = "Response Code: \(response.responseCode!) \n Message Text: \(response.message!) \n Context: \(response.context!) \n Data: \(response.data!)";
    }


}

