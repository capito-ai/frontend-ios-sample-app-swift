//
//  CAPViewController.swift
//  SampleApp
//
//  Created by James Gartland on 20/02/2017.
//  Copyright © 2017 g1653004. All rights reserved.
//

import UIKit

class CAPViewController: UIViewController, SpeechDelegate, TextDelegate, TouchDelegate {

    var isRecording = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onMicrophoneClick() {
        if (isRecording) {
            CapitoController.getInstance().cancelTalking();
        } else {
            CapitoController.getInstance().push(toTalk: self, withDialogueContext: [:])
        }
    }
    
    func handleResponse(response: CapitoResponse) {
        if (response.messageType == "WARNING") {
            debugPrint("Got warning message back with response code: ", response.responseCode);
            bootstrapView(response: response);
        } else {
            bootstrapView(response: response);
        }
    }
    
    func bootstrapView(response: CapitoResponse) {
        // Process response
        debugPrint("Response Code: ", response.responseCode);
        debugPrint("Message Text: ", response.message);
        debugPrint("Context: ", response.context);
        debugPrint("Data: ", response.data);
        // This is where the app-specific code should be placed to handle the response from the Capito Cloud
    }
    
    // MARK: - SpeechDelegate protocol implementation
    
    func speechControllerDidBeginRecording() {
        debugPrint("speechControllerDidBeginRecording");
        isRecording = true;
        // change microphone image
        // update VUMeter?
    }
    
    func speechControllerDidFinishRecording() {
        debugPrint("speechControllerDidFinishRecording");
        isRecording = false;
        // change microphone image
        // update VUMeter?
    }
    
    func speechControllerProcessing(_ transcription: CapitoTranscription!) {
        debugPrint("speechControllerProcessing");
        // display result with 'transcription.firstResult()'
    }
    
    func speechControllerDidFinish(withResults response: CapitoResponse!) {
        debugPrint("speechControllerDidFinishWithResults");
        handleResponse(response: response);
    }
    
    func speechControllerDidFinishWithError(_ error: Error!) {
        debugPrint("speechControllerDidFinishWithError: ", error);
        // display error message to user
    }
    
    // MARK: - TextDelegate protocol implementation
    
    func textControllerDidFinish(withResults response: CapitoResponse!) {
        debugPrint("textControllerDidFinishWithResults");
        handleResponse(response: response);
    }
    
    func textControllerDidFinishWithError(_ error: Error!) {
        debugPrint("textControllerDidFinishWithError:", error);
        // display error message to user
    }
    
    // MARK: - TouchDelegate protocol implementation
    
    func touchControllerDidFinish(withResults response: CapitoResponse!) {
        debugPrint("touchControllerDidFinishWithResults");
        handleResponse(response: response);
    }
    
    func touchControllerDidFinishWithError(_ error: Error!) {
        debugPrint("touchControllerDidFinishWithError:", error);
        // display error message to user.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
