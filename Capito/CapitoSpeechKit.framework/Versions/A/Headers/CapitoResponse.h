//
//  CapitoResponse.h
//  (C) Capito Systems Ltd 2013, All Rights Reserved.
//
//  This class is the device API for communicating
//  with the Capito Cloud
//
//  Created by Darren Harris on 3/14/13.
//
//

#import <Foundation/Foundation.h>

@interface CapitoResponse : NSObject

@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *messageType;
@property (strong, nonatomic) NSNumber *responseCode;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *task;
@property (strong, nonatomic) NSString *domain;
@property (strong, nonatomic) NSString *inputText;
@property (strong, nonatomic) NSArray  *asrs;
@property (strong, nonatomic) NSString *objectId;
@property (strong, nonatomic) NSString *eventType;
@property (strong, nonatomic) NSString *commandType;
@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSDictionary *context;

-(NSString *) description;
-(NSArray *)keys;
@end
