# README #

A basic Swift implementation of the Capito SampleApp, courtesy of James Gartland, Imperial College London. It does not have the full functionality of the original SampleApp (such as the text and touch protocols) but bridges the existing library into Swift. It shows how one could go about writing a base class to inherit Capito functionality (found in Capito>CAPViewController.swift).

Cloned from
https://gitlab.doc.ic.ac.uk/jg916/frontend-ios-sample-app-swift